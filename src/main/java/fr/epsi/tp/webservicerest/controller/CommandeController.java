package fr.epsi.tp.webservicerest.controller;

import fr.epsi.tp.webservicerest.model.Commande;
import fr.epsi.tp.webservicerest.repository.ArticleRepository;
import fr.epsi.tp.webservicerest.repository.CommandeRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CommandeController {


    private final CommandeRepository repository;

    private final ArticleRepository articleRepository;

    public CommandeController(CommandeRepository repository, ArticleRepository articleRepository) {
        this.repository = repository;
        this.articleRepository = articleRepository;
    }

    // Endpoint to add a new commande
    @PostMapping("/commandes")
    public Commande addCommande(@RequestBody Commande commande) {
        commande.getArticles().forEach((article, quantity) -> {
            article.setQuantity(article.getQuantity() - quantity);
            articleRepository.save(article);
        });
        return repository.save(commande);
    }

    // Endpoint to get all commandes
    @GetMapping("/commandes")
    public List<Commande> getAllCommandes() {
        return repository.findAll();
    }

    // Add other necessary endpoints as required.
}
