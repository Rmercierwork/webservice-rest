package fr.epsi.tp.webservicerest.controller;

import fr.epsi.tp.webservicerest.repository.ArticleRepository;
import org.springframework.web.bind.annotation.*;

import fr.epsi.tp.webservicerest.model.Article;

import java.util.List;

@RestController
public class ArticleController {


    private final ArticleRepository repository;

    public ArticleController(ArticleRepository repository) {
        this.repository = repository;
    }

    // Endpoint to add a new article
    @PostMapping("/articles")
    public Article addArticle(@RequestBody Article article) {
        return repository.save(article);
    }

    // Endpoint to get all articles
    @GetMapping("/articles")
    public List<Article> getAllArticles() {
        return repository.findAll();
    }

    // Endpoint to update an article
    @PutMapping("/articles/{id}")
    public Article updateArticle(@RequestBody Article newArticle, @PathVariable Long id) {
        return repository.findById(id)
                .map(article -> {
                    article.setDesignation(newArticle.getDesignation());
                    article.setQuantity(newArticle.getQuantity());
                    article.setPrice(newArticle.getPrice());
                    return repository.save(article);
                })
                .orElseGet(() -> {
                    newArticle.setId(id);
                    return repository.save(newArticle);
                });
    }

    // Endpoint to delete an article
    @DeleteMapping("/articles/{id}")
    public void deleteArticle(@PathVariable Long id) {
        repository.deleteById(id);
    }
}
