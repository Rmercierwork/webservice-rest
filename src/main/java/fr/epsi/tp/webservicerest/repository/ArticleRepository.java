package fr.epsi.tp.webservicerest.repository;

import fr.epsi.tp.webservicerest.model.Article;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ArticleRepository extends JpaRepository<Article, Long> {
}
