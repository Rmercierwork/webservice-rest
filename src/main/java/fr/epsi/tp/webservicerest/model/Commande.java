package fr.epsi.tp.webservicerest.model;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;

@Entity
@Table
public class Commande {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ElementCollection
    @CollectionTable(name = "commande_articles", joinColumns = @JoinColumn(name = "commande_id"))
    @MapKeyJoinColumn(name = "article_id")
    @Column(name = "quantity")
    private Map<Article, Integer> articles = new HashMap<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Map<Article, Integer> getArticles() {
        return articles;
    }

    public void setArticles(Map<Article, Integer> articles) {
        this.articles = articles;
    }
}
